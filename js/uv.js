function consultaDiaUv(dia) {
    var categorias = [];
    var datos = [];
    $('#fechaUv').val(dia);
    db.collection("Datos").where("fecha", "==", dia).orderBy("id").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            categorias.push(doc.data().hora);
            datos.push(doc.data().uv);
        });
        renderGraficoUv(datos, categorias);
        getUvMaxima(datos);
        getUvMinima(datos);
        getPromedioUv(datos);
    });


}

function renderGraficoUv(datos, categorias) {
    $('#ChartUvDia').remove(); // this is my <canvas> element
    $('#graph-container-uv').append('<canvas id="ChartUvDia"><canvas>');
    var ctx = document.getElementById('ChartUvDia').getContext('2d');
    var purple_orange_gradient = ctx.createLinearGradient(0, 0, 0, 600);
    purple_orange_gradient.addColorStop(0, 'purple');
    purple_orange_gradient.addColorStop(0.5, 'yellow');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: categorias,
            datasets: [{
                label: 'Uv Diaria',
                backgroundColor: purple_orange_gradient,
                hoverBackgroundColor: purple_orange_gradient,
                hoverBorderWidth: 5,
                hoverBorderColor: 'purple',
                data: datos
            }]
        },
        // Configuration options go here
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
        }
    });

    chart.update();

}

//Fecha del selector, una vez cambie, se parsea, y se envia la consulta para actualizar los graficos y las visualizaciones
$('#fechaUv').change(function () {

    var fechaUv = new Date(this.value);
    console.log(fechaUv);
    var dia = fechaUv.getDate().toString();
    if (dia.length === 1) {
        console.log("Entro al dia");
        dia = "0" + dia;
    }
    var mes = (parseInt(fechaUv.getMonth()) + 1).toString();
    if (mes.length === 1) {
        console.log("Entro al mes");
        mes = "0" + mes;
    }
    var anio = fechaUv.getFullYear().toString().slice((-2));
    var cont = 0;
    var fechaBusqueda = dia + "-" + mes + "-" + anio;
    consultaDiaUv(fechaBusqueda);
});

$('#consultarMesUv').click(function () {
    alert('Se consultara por mes la uv');
});


function getUvMaxima(listaUv) {

    Array.max = function (auxListaUv) {
        return Math.max.apply(Math, auxListaUv);
    };
    var listaUv = Array.max(listaUv);
    $('#uvMaxima').text(listaUv);

}

function getUvMinima(listaTemperaturas) {

    Array.min = function (auxlistaTemperaturas) {
        return Math.min.apply(Math, auxlistaTemperaturas);
    };
    var temperaturaMinima = Array.min(listaTemperaturas);
    $('#uvMinima').text(temperaturaMinima);

}

function getPromedioUv(listaTemperaturas) {
    var sum = 0;
    listaTemperaturas.forEach(function (valor) {
        sum = (parseFloat(valor)) + sum;
    });
    let avg = sum / listaTemperaturas.length;
    $('#promedioUv').text(Math.round(avg));
}
