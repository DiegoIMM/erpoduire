function consultaDiaViento(dia) {
    var categorias = [];
    var datos = [];
    $('#fechaViento').val(dia);
    db.collection("Datos").where("fecha", "==", dia).orderBy("id").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            categorias.push(doc.data().hora);
            datos.push(doc.data().velocidadViento);
        });
        renderGraficoViento(datos, categorias);
        getVientoMaxima(datos);
        getVientoMinima(datos);
        getPromedioViento(datos);
    });


}

function renderGraficoViento(datos, categorias) {
    $('#ChartVientoDia').remove(); // this is my <canvas> element
    $('#graph-container-viento').append('<canvas id="ChartVientoDia"><canvas>');
    var ctx = document.getElementById('ChartVientoDia').getContext('2d');
    var purple_orange_gradient = ctx.createLinearGradient(0, 0, 0, 600);
    purple_orange_gradient.addColorStop(0, 'cyan');
    purple_orange_gradient.addColorStop(0.5, 'blue');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: categorias,
            datasets: [{
                label: 'Viento (Millas/Hora)',
                backgroundColor: purple_orange_gradient,
                hoverBackgroundColor: purple_orange_gradient,
                hoverBorderWidth: 5,
                hoverBorderColor: 'purple',
                data: datos
            }]
        },
        // Configuration options go here
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
        }
    });

    chart.update();

}

//Fecha del selector, una vez cambie, se parsea, y se envia la consulta para actualizar los graficos y las visualizaciones
$('#fechaViento').change(function () {

    var fechaViento = new Date(this.value);
    console.log(fechaViento);
    var dia = fechaViento.getDate().toString();
    if (dia.length === 1) {
        console.log("Entro al dia");
        dia = "0" + dia;
    }
    var mes = (parseInt(fechaViento.getMonth()) + 1).toString();
    if (mes.length === 1) {
        console.log("Entro al mes");
        mes = "0" + mes;
    }
    var anio = fechaViento.getFullYear().toString().slice((-2));
    var cont = 0;
    var fechaBusqueda = dia + "-" + mes + "-" + anio;
    consultaDiaViento(fechaBusqueda);
});

$('#consultarMesViento').click(function () {
    alert('Se consultara por mes la viento');
});


function getVientoMaxima(listaViento) {

    Array.max = function (auxListaViento) {
        return Math.max.apply(Math, auxListaViento);
    };
    var listaViento = Array.max(listaViento);
    $('#vientoMaxima').text(listaViento.toString() + 'M/S');

}

function getVientoMinima(listaTemperaturas) {

    Array.min = function (auxlistaTemperaturas) {
        return Math.min.apply(Math, auxlistaTemperaturas);
    };
    var temperaturaMinima = Array.min(listaTemperaturas);
    $('#vientoMinima').text(temperaturaMinima.toString() + 'M/S');

}

function getPromedioViento(listaTemperaturas) {
    var sum = 0;
    listaTemperaturas.forEach(function (valor) {
        sum = (parseFloat(valor)) + sum;
    });
    let avg = sum / listaTemperaturas.length;
    $('#promedioViento').text(Math.round(avg).toString() + 'M/S');
}
