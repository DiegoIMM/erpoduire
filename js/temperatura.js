function consultaDiaTemperatura(dia) {
    var categorias = [];
    var datos = [];
    $('#fechaTemp').val(dia);
    db.collection("Datos").where("fecha", "==", dia).orderBy("id").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            categorias.push(doc.data().hora);
            datos.push(doc.data().temperatura);
        });
        renderGraficoTemperatura(datos, categorias);
        $('#temperaturaMaxima').text(getTemperaturaMaxima(datos));
        $('#temperaturaMinima').text(getTemperaturaMinima(datos));
        $('#promedioTemperatura').text(getPromedioTemperatura(datos));

    });


}

$('#exportarTemperaturaDia').click(function () {
    var element = document.getElementById('pdfTempDia');
    var opt = {
        margin: 0.1,
        filename: 'myfile.pdf',
        image: {type: 'jpeg', quality: 0.99},
        html2canvas: {scale: 1},
        jsPDF: {unit: 'in', format: 'legal', orientation: 'landscape'}
    };

    html2pdf(element, opt);
});

function renderGraficoTemperatura(datos, categorias) {
    $('#ChartTempDia').remove(); // this is my <canvas> element
    $('#graph-container-temp').append('<canvas id="ChartTempDia"><canvas>');
    var ctx = document.getElementById('ChartTempDia').getContext('2d');
    var purple_orange_gradient = ctx.createLinearGradient(0, 0, 0, 600);
    purple_orange_gradient.addColorStop(0, 'orange');
    purple_orange_gradient.addColorStop(0.5, 'cyan');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: categorias,
            datasets: [{
                label: 'Temperatura',
                backgroundColor: purple_orange_gradient,
                hoverBackgroundColor: purple_orange_gradient,
                hoverBorderWidth: 5,
                hoverBorderColor: 'green',
                data: datos
            }]
        },
        // Configuration options go here
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
        }
    });


    chart.update();

}

function renderGraficoTemperaturaMes(datos, categorias) {
    $('#ChartTempMes').remove(); // this is my <canvas> element
    $('#graph-container-tempmes').append('<canvas id="ChartTempMes"><canvas>');
    var ctx = document.getElementById('ChartTempMes').getContext('2d');
    var purple_orange_gradient = ctx.createLinearGradient(0, 0, 0, 600);
    purple_orange_gradient.addColorStop(0, 'orange');
    purple_orange_gradient.addColorStop(0.5, 'cyan');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: categorias,
            datasets: [{
                label: 'Temperatura',
                backgroundColor: purple_orange_gradient,
                hoverBackgroundColor: purple_orange_gradient,
                hoverBorderWidth: 5,
                hoverBorderColor: 'green',
                data: datos
            }]
        },
        // Configuration options go here
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
        }
    });


    chart.update();

}

function renderGraficoTemperaturaAno(datos, categorias) {
    $('#ChartTempAno').remove(); // this is my <canvas> element
    $('#graph-container-tempano').append('<canvas id="ChartTempAno"><canvas>');
    var ctx = document.getElementById('ChartTempAno').getContext('2d');
    var purple_orange_gradient = ctx.createLinearGradient(0, 0, 0, 600);
    purple_orange_gradient.addColorStop(0, 'orange');
    purple_orange_gradient.addColorStop(0.5, 'cyan');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: categorias,
            datasets: [{
                label: 'Temperatura (ºC)',
                backgroundColor: purple_orange_gradient,
                hoverBackgroundColor: purple_orange_gradient,
                hoverBorderWidth: 5,
                hoverBorderColor: 'green',
                data: datos
            }]
        },
        // Configuration options go here
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
        }
    });


    chart.update();

}

//Fecha del selector, una vez cambie, se parsea, y se envia la consulta para actualizar los graficos y las visualizaciones
$('#fechaTemp').change(function () {

    var fechaTemp = new Date(this.value);
    console.log(fechaTemp);
    var dia = fechaTemp.getDate().toString();
    if (dia.length === 1) {
        console.log("Entro al dia");
        dia = "0" + dia;
    }
    var mes = (parseInt(fechaTemp.getMonth()) + 1).toString();
    if (mes.length === 1) {
        console.log("Entro al mes");
        mes = "0" + mes;
    }
    var anio = fechaTemp.getFullYear().toString().slice((-2));
    var cont = 0;
    var fechaBusqueda = dia + "-" + mes + "-" + anio;
    consultaDiaTemperatura(fechaBusqueda);
});

$('#consultarMesTemperatura').click(function () {
    var datos = [];
    var categorias = [];

    var mesTemperatura = $('#fechaMesTemperatura option:selected').val();
    var anoTemperatura = $('#fechaAnoTemperatura option:selected').val();
    var mesConsultar = mesTemperatura + anoTemperatura;
    console.log(mesConsultar);


    db.collection("Datos").where("mes", "==", mesConsultar).orderBy("id").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.warn(doc.id, " => ", doc.data());
            categorias.push(doc.data().dia);
            datos.push(doc.data().temperatura);
        });
        renderGraficoTemperaturaMes(datos, categorias);
    });


});

$('#consultarAnoTemperatura').click(function () {
    var datos = [];
    var categorias = [];

    var anoTemperatura = $('#fechaSoloAnoTemperatura option:selected').val();


    db.collection("Datos").where("anio", "==", anoTemperatura).orderBy("id").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.warn(doc.id, " => ", doc.data());
            categorias.push(doc.data().mes);
            datos.push(doc.data().temperatura);
        });
        renderGraficoTemperaturaAno(datos, categorias);
    });


});



function getTemperaturaMaxima(listaTemperaturas) {
    Array.max = function (auxlistaTemperaturas) {
        return Math.max.apply(Math, auxlistaTemperaturas);
    };
    return Array.max(listaTemperaturas).toString() + 'º'
}

function getTemperaturaMinima(listaTemperaturas) {
    Array.min = function (auxlistaTemperaturas) {
        return Math.min.apply(Math, auxlistaTemperaturas);
    };
    return Array.min(listaTemperaturas).toString() + 'º';

}

function getPromedioTemperatura(listaTemperaturas) {
    var sum = 0;
    listaTemperaturas.forEach(function (valor) {
        sum = (parseFloat(valor)) + sum;
    });
    let avg = sum / listaTemperaturas.length;
    return Math.round(avg).toString() + 'º'
}
