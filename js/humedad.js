function consultaDiaHumedad(dia) {
    var categorias = [];
    var datos = [];
    $('#fechaHumedad').val(dia);
    db.collection("Datos").where("fecha", "==", dia).orderBy("id").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            categorias.push(doc.data().hora);
            datos.push(doc.data().humedad);
        });
        renderGraficoHumedad(datos, categorias);
        getHumedadMaxima(datos);
        getHumedadMinima(datos);
        getPromedioHumedad(datos);
    });


}

function renderGraficoHumedad(datos, categorias) {
    $('#ChartHumedadDia').remove(); // this is my <canvas> element
    $('#graph-container-humedad').append('<canvas id="ChartHumedadDia"><canvas>');
    var ctx = document.getElementById('ChartHumedadDia').getContext('2d');
    var purple_orange_gradient = ctx.createLinearGradient(0, 0, 0, 600);
    purple_orange_gradient.addColorStop(0, 'green');
    purple_orange_gradient.addColorStop(0.5, 'cyan');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: categorias,
            datasets: [{
                label: 'Humedad Diaria',
                backgroundColor: purple_orange_gradient,
                hoverBackgroundColor: purple_orange_gradient,
                hoverBorderWidth: 5,
                hoverBorderColor: 'purple',
                data: datos
            }]
        },
        // Configuration options go here
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
        }
    });

    chart.update();

}

//Fecha del selector, una vez cambie, se parsea, y se envia la consulta para actualizar los graficos y las visualizaciones
$('#fechaHumedad').change(function () {

    var fechaHumedad = new Date(this.value);
    console.log(fechaHumedad);
    var dia = fechaHumedad.getDate().toString();
    if (dia.length === 1) {
        console.log("Entro al dia");
        dia = "0" + dia;
    }
    var mes = (parseInt(fechaHumedad.getMonth()) + 1).toString();
    if (mes.length === 1) {
        console.log("Entro al mes");
        mes = "0" + mes;
    }
    var anio = fechaHumedad.getFullYear().toString().slice((-2));
    var cont = 0;
    var fechaBusqueda = dia + "-" + mes + "-" + anio;
    consultaDiaHumedad(fechaBusqueda);
});

$('#consultarMesHumedad').click(function () {
    alert('Se consultara por mes la humedad');
});


function getHumedadMaxima(listaHumedad) {

    Array.max = function (auxListaHumedad) {
        return Math.max.apply(Math, auxListaHumedad);
    };
    var listaHumedad = Array.max(listaHumedad);
    $('#humedadMaxima').text(listaHumedad);

}

function getHumedadMinima(listaTemperaturas) {

    Array.min = function (auxlistaTemperaturas) {
        return Math.min.apply(Math, auxlistaTemperaturas);
    };
    var temperaturaMinima = Array.min(listaTemperaturas);
    $('#humedadMinima').text(temperaturaMinima);

}

function getPromedioHumedad(listaTemperaturas) {
    var sum = 0;
    listaTemperaturas.forEach(function (valor) {
        sum = (parseFloat(valor)) + sum;
    });
    let avg = sum / listaTemperaturas.length;
    $('#promedioHumedad').text(Math.round(avg));
}
