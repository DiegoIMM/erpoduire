function consultaDiaLluvia(dia) {
    var categorias = [];
    var datos = [];
    $('#fechaLluvia').val(dia);
    db.collection("Datos").where("fecha", "==", dia).orderBy("id").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            categorias.push(doc.data().hora);
            datos.push(doc.data().lluvia);
        });
        renderGraficoLluvia(datos, categorias);
        getLluviaMaxima(datos);
        getLluviaMinima(datos);
        getPromedioLluvia(datos);
    });


}

function renderGraficoLluvia(datos, categorias) {
    $('#ChartLluviaDia').remove(); // this is my <canvas> element
    $('#graph-container-lluvia').append('<canvas id="ChartLluviaDia"><canvas>');
    var ctx = document.getElementById('ChartLluviaDia').getContext('2d');
    var purple_orange_gradient = ctx.createLinearGradient(0, 0, 0, 600);
    purple_orange_gradient.addColorStop(0, 'blue');
    purple_orange_gradient.addColorStop(0.5, 'cyan');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: categorias,
            datasets: [{
                label: 'Lluvia Diaria',
                backgroundColor: purple_orange_gradient,
                hoverBackgroundColor: purple_orange_gradient,
                hoverBorderWidth: 5,
                hoverBorderColor: 'purple',
                data: datos
            }]
        },
        // Configuration options go here
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
        }
    });

    chart.update();

}

//Fecha del selector, una vez cambie, se parsea, y se envia la consulta para actualizar los graficos y las visualizaciones
$('#fechaLluvia').change(function () {

    var fechaLluvia = new Date(this.value);
    console.log(fechaLluvia);
    var dia = fechaLluvia.getDate().toString();
    if (dia.length === 1) {
        console.log("Entro al dia");
        dia = "0" + dia;
    }
    var mes = (parseInt(fechaLluvia.getMonth()) + 1).toString();
    if (mes.length === 1) {
        console.log("Entro al mes");
        mes = "0" + mes;
    }
    var anio = fechaLluvia.getFullYear().toString().slice((-2));
    var cont = 0;
    var fechaBusqueda = dia + "-" + mes + "-" + anio;
    consultaDiaLluvia(fechaBusqueda);
});

$('#consultarMesLluvia').click(function () {
    alert('Se consultara por mes la lluvia');
});


function getLluviaMaxima(listaLluvia) {

    Array.max = function (auxListaLluvia) {
        return Math.max.apply(Math, auxListaLluvia);
    };
    var listaLluvia = Array.max(listaLluvia);
    $('#lluviaMaxima').text(listaLluvia);

}

function getLluviaMinima(listaTemperaturas) {

    Array.min = function (auxlistaTemperaturas) {
        return Math.min.apply(Math, auxlistaTemperaturas);
    };
    var temperaturaMinima = Array.min(listaTemperaturas);
    $('#lluviaMinima').text(temperaturaMinima);

}

function getPromedioLluvia(listaTemperaturas) {
    var sum = 0;
    listaTemperaturas.forEach(function (valor) {
        sum = (parseFloat(valor)) + sum;
    });
    let avg = sum / listaTemperaturas.length;
    $('#promedioLluvia').text(Math.round(avg));
}
