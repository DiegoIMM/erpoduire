// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyADohYg7iOB2nqEXZ9Qq3IuAfUk1YJZez4",
    authDomain: "erpoduire.firebaseapp.com",
    databaseURL: "https://erpoduire.firebaseio.com",
    projectId: "erpoduire",
    storageBucket: "erpoduire.appspot.com",
    messagingSenderId: "82114508837",
    appId: "1:82114508837:web:9c74925c269fe2d6c6a7cc",
    measurementId: "G-05LRVRB765"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();
var db = firebase.firestore();
obtenerUltimoDia();

function obtenerUltimoDia() {
    var ultimoDia;
    db.collection("Datos").orderBy("id", "desc").limit(2).get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            ultimoDia = doc.data().fecha;

        });
        consultaDiaTemperatura(ultimoDia);
        consultaDiaEnergia(ultimoDia);
        consultaDiaBarometro(ultimoDia);
        consultaDiaRadiacion(ultimoDia);
        consultaDiaUv(ultimoDia);
        consultaDiaHumedad(ultimoDia);
        consultaDiaLluvia(ultimoDia);
        consultaDiaViento(ultimoDia);
    });
}

$('#inicarSesion').click(function () {
    var email = $('#email').val();
    var password = $('#password').val();
    console.log('Se ha iniciado Sesion');
    firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // ...
    });


});

$('#cerrarSesion').click(function () {
    firebase.auth().signOut().then(function () {
        console.log('Sesion Cerrada');
        // Sign-out successful.
    }).catch(function (error) {
        // An error happened.
    });
});


firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        $('#subir').removeClass('hide');
        $('#cerrarSesion').removeClass('hide');
        $('#botonModal').addClass('hide');

    } else {
        // User is signed out.
        // ...
        $('#subir').addClass('hide');
        $('#cerrarSesion').addClass('hide');
        $('#botonModal').removeClass('hide');

    }
});




