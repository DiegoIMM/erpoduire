// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyADohYg7iOB2nqEXZ9Qq3IuAfUk1YJZez4",
    authDomain: "erpoduire.firebaseapp.com",
    databaseURL: "https://erpoduire.firebaseio.com",
    projectId: "erpoduire",
    storageBucket: "erpoduire.appspot.com",
    messagingSenderId: "82114508837",
    appId: "1:82114508837:web:9c74925c269fe2d6c6a7cc",
    measurementId: "G-05LRVRB765"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();
var db = firebase.firestore();
obtenerUltimoDia();
getUltimoArchivo();

$('#upload').click(function () {

    console.log($("#file").prop('files')[0].name);

    var datos = [];
    //Obtengo el archivo completo, como es uno solo, es la posicion 0 del array.
    var file = $("#file").prop('files')[0];
    //La funcion Papa.parse, recorre el archivo linea por linea y dentro de ella encontramos el array en result.data


    Papa.parse(file, {
        complete: function (results) {
            console.log("Finished:");
            results.data.forEach(function (element, index) {
                if (index > 1 && element.length > 2) {
                    var dia = element[0].toString().substring(0, 2);
                    var mes = element[0].toString().substring(3, 5);
                    var anio = "20" + element[0].toString().substring(6, 8);
                    var hora = element[1].toString().substring(0, 2);
                    if (hora.includes(":")) {
                        hora = "0" + hora.substring(0, 1);
                    }
                    var minutos = element[1].toString().substring(3, 5);
                    var fecha = new Date(anio, mes, dia, hora, minutos);
                    // var fechaHora = element[0] + "-" + element[1];

                    //console.warn(fechaHora);
                    var aux = {
                        id: fecha,
                        fecha: element[0],
                        dia: dia,
                        mes: mes + anio,
                        anio: anio,
                        hora: element[1],
                        temperatura: element[2],
                        humedad: element[5],
                        velocidadViento: element[7],
                        direccionViento: element[8],
                        barometro: element[16],
                        lluvia: element[17],
                        radiacionSolar: element[21],
                        energiaSolar: element[20],
                        uv: element[22]

                    };
                    datos.push(aux);
                }
            });
            console.log(datos);
            var c = 0;
            var porcentaje = 0;


            datos.forEach(function (element) {
                // Se agregan los datos uno por uno a la base de datos

                db.collection("Datos").doc(element.id.toString()).set(element)
                    .then(function () {
                        console.log("Documento escrito exitosamente");
                        c += 1;
                        porcentaje = c * 100 / datos.length;
                        $('#Cargando').append('<div class="determinate" style="width:' + porcentaje
                            + '%"></div>');
                        if (porcentaje === 100) {
                            datosCargados();


                        }
                    })
                    .catch(function (error) {
                        console.error("Error adding document: ", error);
                    });
            });

            $('#previewData').text("Se subiran un total de : " + datos.length + " datos");


        }
    });
});

function datosCargados() {
    $('#Cargando').text('Datos cargados correctamente');
    M.toast({html: 'Se han cargado los datos exitosamente', classes: 'green'});
    // Guardamos el nombre del ultimo archivo para mostrarlo en pantalla la siguiente vez
    db.collection("Info").doc('ultimoElemento').set({
        archivo: $("#file").prop('files')[0].name,
        timestamp: firebase.firestore.FieldValue.serverTimestamp()
    })
        .then(function () {
            console.log("Nombre ultimo elemento guardado");
            getUltimoArchivo();
            obtenerUltimoDia();
            $('#file').val(null);
            $('#filelabel').val(null);

        }).catch(function (error) {
        console.error("No se guardo el nombre ", error);
    });
}

function obtenerUltimoDia() {
    var fecha = "No hay datos cargados en la bdd";
    db.collection("Datos").orderBy("id", "desc").limit(1).get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            fecha = doc.data().fecha;

        });
        $('#ultimoDia').text("Se debe subir desde el dia: " + fecha);

    });
}


function getUltimoArchivo() {
    var archivo = "No se ha subido ningun archivo";
    var when = "No se ha subido ningun archivo";

    db.collection("Info").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.warn(doc.id, " => ", doc.data());
            archivo = doc.data().archivo;
            when = doc.data().timestamp;

        });
        $('#ultimoArchivo').text("El ultimo archivo fue: " + archivo + " el dia: " + when.toDate().getDate() + " de " + (parseInt(when.toDate().getMonth()) + 1) + " del " + when.toDate().getFullYear());

    });
}
