function consultaDiaEnergia(dia) {
    var categorias = [];
    var datos = [];
    $('#fechaEnergia').val(dia);
    db.collection("Datos").where("fecha", "==", dia).orderBy("id").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            categorias.push(doc.data().hora);
            datos.push(doc.data().energiaSolar);
        });
        renderGraficoEnergia(datos, categorias);
        getEnergiaMaxima(datos);
        getEnergiaMinima(datos);
        getPromedioEnergia(datos);
    });


}

function renderGraficoEnergia(datos, categorias) {
    $('#ChartEnergiaDia').remove(); // this is my <canvas> element
    $('#graph-container-energia').append('<canvas id="ChartEnergiaDia"><canvas>');
    var ctx = document.getElementById('ChartEnergiaDia').getContext('2d');
    var purple_orange_gradient = ctx.createLinearGradient(0, 0, 0, 600);
    purple_orange_gradient.addColorStop(0, 'orange');
    purple_orange_gradient.addColorStop(0.5, 'yellow');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: categorias,
            datasets: [{
                label: 'Energia Diaria (LY)',
                backgroundColor: purple_orange_gradient,
                hoverBackgroundColor: purple_orange_gradient,
                hoverBorderWidth: 5,
                hoverBorderColor: 'purple',
                data: datos
            }]
        },
        // Configuration options go here
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
        }
    });

    chart.update();

}

function renderGraficoEnergiaMes(datos, categorias) {
    $('#ChartEnergiaMes').remove(); // this is my <canvas> element
    $('#graph-container-energiames').append('<canvas id="ChartEnergiaMes"><canvas>');
    var ctx = document.getElementById('ChartEnergiaMes').getContext('2d');
    var purple_orange_gradient = ctx.createLinearGradient(0, 0, 0, 600);
    purple_orange_gradient.addColorStop(0, 'orange');
    purple_orange_gradient.addColorStop(0.5, 'cyan');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: categorias,
            datasets: [{
                label: 'Temperatura',
                backgroundColor: purple_orange_gradient,
                hoverBackgroundColor: purple_orange_gradient,
                hoverBorderWidth: 5,
                hoverBorderColor: 'green',
                data: datos
            }]
        },
        // Configuration options go here
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
        }
    });


    chart.update();

}


//Fecha del selector, una vez cambie, se parsea, y se envia la consulta para actualizar los graficos y las visualizaciones
$('#fechaEnergia').change(function () {

    var fechaEnergia = new Date(this.value);
    console.log(fechaEnergia);
    var dia = fechaEnergia.getDate().toString();
    if (dia.length === 1) {
        console.log("Entro al dia");
        dia = "0" + dia;
    }
    var mes = (parseInt(fechaEnergia.getMonth()) + 1).toString();
    if (mes.length === 1) {
        console.log("Entro al mes");
        mes = "0" + mes;
    }
    var anio = fechaEnergia.getFullYear().toString().slice((-2));
    var cont = 0;
    var fechaBusqueda = dia + "-" + mes + "-" + anio;
    consultaDiaEnergia(fechaBusqueda);
});

$('#consultarMesEnergia').click(function () {
    var datos = [];
    var categorias = [];

    var mesTemperatura = $('#fechaMesEnergia option:selected').val();
    var anoTemperatura = $('#fechaAnoEnergia option:selected').val();
    var mesConsultar = mesTemperatura + anoTemperatura;
    console.log(mesConsultar);


    db.collection("Datos").where("mes", "==", mesConsultar).orderBy("id").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.warn(doc.id, " => ", doc.data());
            categorias.push(doc.data().dia);
            datos.push(doc.data().temperatura);
        });
        renderGraficoEnergiaMes(datos, categorias);
    });


});

function getEnergiaMaxima(listaEnergia) {

    Array.max = function (auxListaEnergia) {
        return Math.max.apply(Math, auxListaEnergia);
    };
    var listaEnergia = Array.max(listaEnergia);
    $('#energiaMaxima').text(listaEnergia);

}

function getEnergiaMinima(listaTemperaturas) {

    Array.min = function (auxlistaTemperaturas) {
        return Math.min.apply(Math, auxlistaTemperaturas);
    };
    var temperaturaMinima = Array.min(listaTemperaturas);
    $('#energiaMinima').text(temperaturaMinima);

}

function getPromedioEnergia(listaTemperaturas) {
    var sum = 0;
    listaTemperaturas.forEach(function (valor) {
        sum = (parseFloat(valor)) + sum;
    });
    let avg = sum / listaTemperaturas.length;
    $('#promedioEnergia').text(Math.round(avg));
}
