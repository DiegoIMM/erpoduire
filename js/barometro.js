function consultaDiaBarometro(dia) {
    var categorias = [];
    var datos = [];
    $('#fechaBarometro').val(dia);
    db.collection("Datos").where("fecha", "==", dia).orderBy("id").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            categorias.push(doc.data().hora);
            datos.push(doc.data().barometro);
        });
        renderGraficoBarometro(datos, categorias);
        getBarometroMaxima(datos);
        getBarometroMinima(datos);
        getPromedioBarometro(datos);
    });


}

function renderGraficoBarometro(datos, categorias) {
    $('#ChartBarometroDia').remove(); // this is my <canvas> element
    $('#graph-container-barometro').append('<canvas id="ChartBarometroDia"><canvas>');
    var ctx = document.getElementById('ChartBarometroDia').getContext('2d');
    var purple_orange_gradient = ctx.createLinearGradient(0, 0, 0, 600);
    purple_orange_gradient.addColorStop(0, 'red');
    purple_orange_gradient.addColorStop(0.5, 'yellow');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: categorias,
            datasets: [{
                label: 'Barometro Diaria',
                backgroundColor: purple_orange_gradient,
                hoverBackgroundColor: purple_orange_gradient,
                hoverBorderWidth: 5,
                hoverBorderColor: 'purple',
                data: datos
            }]
        },
        // Configuration options go here
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
        }
    });

    chart.update();

}

//Fecha del selector, una vez cambie, se parsea, y se envia la consulta para actualizar los graficos y las visualizaciones
$('#fechabarometro').change(function () {

    var fechaBarometro = new Date(this.value);
    console.log(fechaBarometro);
    var dia = fechaBarometro.getDate().toString();
    if (dia.length === 1) {
        console.log("Entro al dia");
        dia = "0" + dia;
    }
    var mes = (parseInt(fechaBarometro.getMonth()) + 1).toString();
    if (mes.length === 1) {
        console.log("Entro al mes");
        mes = "0" + mes;
    }
    var anio = fechaBarometro.getFullYear().toString().slice((-2));
    var cont = 0;
    var fechaBusqueda = dia + "-" + mes + "-" + anio;
    consultaDiaBarometro(fechaBusqueda);
});

$('#consultarMesBarometro').click(function () {
    alert('Se consultara por mes la barometro');
});


function getBarometroMaxima(listaBarometro) {

    Array.max = function (auxListaBarometro) {
        return Math.max.apply(Math, auxListaBarometro);
    };
    var listaBarometro = Array.max(listaBarometro);
    $('#barometroMaxima').text(listaBarometro);

}

function getBarometroMinima(listaTemperaturas) {

    Array.min = function (auxlistaTemperaturas) {
        return Math.min.apply(Math, auxlistaTemperaturas);
    };
    var temperaturaMinima = Array.min(listaTemperaturas);
    $('#barometroMinima').text(temperaturaMinima);

}

function getPromedioBarometro(listaTemperaturas) {
    var sum = 0;
    listaTemperaturas.forEach(function (valor) {
        sum = (parseFloat(valor)) + sum;
    });
    let avg = sum / listaTemperaturas.length;
    $('#promedioBarometro').text(Math.round(avg));
}
