$('#fechaInicio').change(function () {
    var fechaInicio = new Date(this.value);
    console.log(fechaInicio);
    var dia = fechaInicio.getDate().toString();
    if (dia.length === 1) {
        console.log("Entro al dia");
        dia = "0" + dia;
    }
    var mes = (parseInt(fechaInicio.getMonth()) + 1).toString();
    if (mes.length === 1) {
        console.log("Entro al mes");
        mes = "0" + mes;
    }
    var anio = fechaInicio.getFullYear().toString().slice((-2));
    var cont = 0;
    var fechaBusqueda = dia + "-" + mes + "-" + anio;
    console.log(fechaBusqueda);
    db.collection("Datos").where("fecha", "==", fechaBusqueda)
        .get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                cont++;
                // doc.data() is never undefined for query doc snapshots
                console.log(doc.id, " => ", doc.data());
            });
            console.log(cont);

        })

        .catch(function (error) {
            console.log("Error getting documents: ", error);
        });
});
