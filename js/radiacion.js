function consultaDiaRadiacion(dia) {
    var categorias = [];
    var datos = [];
    $('#fechaRadiacion').val(dia);
    db.collection("Datos").where("fecha", "==", dia).orderBy("id").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            categorias.push(doc.data().hora);
            datos.push(doc.data().radiacionSolar);
        });
        renderGraficoRadiacion(datos, categorias);
        getRadiacionMaxima(datos);
        getRadiacionMinima(datos);
        getPromedioRadiacion(datos);
    });


}

function renderGraficoRadiacion(datos, categorias) {
    $('#ChartRadiacionDia').remove(); // this is my <canvas> element
    $('#graph-container-radiacion').append('<canvas id="ChartRadiacionDia"><canvas>');
    var ctx = document.getElementById('ChartRadiacionDia').getContext('2d');
    var purple_orange_gradient = ctx.createLinearGradient(0, 0, 0, 600);
    purple_orange_gradient.addColorStop(0, 'orange');
    purple_orange_gradient.addColorStop(0.5, 'yellow');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: categorias,
            datasets: [{
                label: 'Radiacion Diaria',
                backgroundColor: purple_orange_gradient,
                hoverBackgroundColor: purple_orange_gradient,
                hoverBorderWidth: 5,
                hoverBorderColor: 'purple',
                data: datos
            }]
        },
        // Configuration options go here
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
        }
    });

    chart.update();

}

//Fecha del selector, una vez cambie, se parsea, y se envia la consulta para actualizar los graficos y las visualizaciones
$('#fechaRadiacion').change(function () {

    var fechaRadiacion = new Date(this.value);
    console.log(fechaRadiacion);
    var dia = fechaRadiacion.getDate().toString();
    if (dia.length === 1) {
        console.log("Entro al dia");
        dia = "0" + dia;
    }
    var mes = (parseInt(fechaRadiacion.getMonth()) + 1).toString();
    if (mes.length === 1) {
        console.log("Entro al mes");
        mes = "0" + mes;
    }
    var anio = fechaRadiacion.getFullYear().toString().slice((-2));
    var cont = 0;
    var fechaBusqueda = dia + "-" + mes + "-" + anio;
    consultaDiaRadiacion(fechaBusqueda);
});

$('#consultarMesRadiacion').click(function () {
    alert('Se consultara por mes la radiacion');
});


function getRadiacionMaxima(listaRadiacion) {

    Array.max = function (auxListaRadiacion) {
        return Math.max.apply(Math, auxListaRadiacion);
    };
    var listaRadiacion = Array.max(listaRadiacion);
    $('#radiacionMaxima').text(listaRadiacion);

}

function getRadiacionMinima(listaTemperaturas) {

    Array.min = function (auxlistaTemperaturas) {
        return Math.min.apply(Math, auxlistaTemperaturas);
    };
    var temperaturaMinima = Array.min(listaTemperaturas);
    $('#radiacionMinima').text(temperaturaMinima);

}

function getPromedioRadiacion(listaTemperaturas) {
    var sum = 0;
    listaTemperaturas.forEach(function (valor) {
        sum = (parseFloat(valor)) + sum;
    });
    let avg = sum / listaTemperaturas.length;
    $('#promedioRadiacion').text(Math.round(avg));
}
